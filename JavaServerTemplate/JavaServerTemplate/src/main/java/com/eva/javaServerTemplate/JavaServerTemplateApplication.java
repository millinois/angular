package com.eva.javaServerTemplate;

import com.eva.javaServerTemplate.datatablesJpa.qrepository.QDataTablesRepositoryFactoryBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(repositoryFactoryBeanClass = QDataTablesRepositoryFactoryBean.class)
@SpringBootApplication
public class JavaServerTemplateApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaServerTemplateApplication.class, args);
	}
}
