package com.eva.javaServerTemplate.controller;

import com.eva.javaServerTemplate.domain.TemplateEntity;
import com.eva.javaServerTemplate.service.TemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/")
public class TemplateController {

    @Autowired
    private TemplateService templateService;


    @GetMapping("getData")
    public List<TemplateEntity> getData() {
        return templateService.findAll();
    }

    @PostMapping("save")
    public TemplateEntity save(@RequestBody TemplateEntity template) {
        return templateService.save(template);
    }

    @PostMapping("delete")
    public ResponseEntity<String> delete(@RequestBody Long templateId) {
        templateService.delete(templateId);
        return new ResponseEntity<>(HttpStatus.OK);
    }


}
