package com.eva.javaServerTemplate.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity
@Getter
@Setter
@NoArgsConstructor
public class TemplateEntity {


    @Builder
    public TemplateEntity(String category, String description, Boolean passed) {

        this.category = category;
        this.description = description;
        this.passed = passed;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long templateId;

    private String category;

    private String description;

    private Boolean passed;

//    private Boolean passed;
}
