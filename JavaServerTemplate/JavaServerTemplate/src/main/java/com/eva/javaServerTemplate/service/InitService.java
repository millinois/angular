package com.eva.javaServerTemplate.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Profile("dev")
@Service
@Transactional
public class InitService {

    @Autowired
    private JsonParseService jsonParseService;

    @PostConstruct
    private void init() throws IOException {
            jsonParseService.init();
    }
}
