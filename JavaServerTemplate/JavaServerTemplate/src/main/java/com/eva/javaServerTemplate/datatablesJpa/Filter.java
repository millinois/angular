package com.eva.javaServerTemplate.datatablesJpa;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.PathBuilder;


interface Filter {
   Predicate createPredicate(PathBuilder<?> pathBuilder, String attributeName);
}