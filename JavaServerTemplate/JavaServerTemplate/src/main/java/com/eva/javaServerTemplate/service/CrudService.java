package com.eva.javaServerTemplate.service;

import java.util.List;
import java.util.Optional;

public interface CrudService<T, Z> {

    Optional<T> findById(Z id);

    List<T> findAll();

    T save(T t);

    List<T> saveAll(List<T> list);

    void delete(Z id);
}
