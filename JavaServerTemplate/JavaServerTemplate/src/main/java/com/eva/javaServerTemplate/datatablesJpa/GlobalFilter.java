package com.eva.javaServerTemplate.datatablesJpa;

import com.querydsl.core.types.Ops;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.core.types.dsl.StringOperation;

/**
 * Filter which creates a basic "WHERE ... LIKE ..." clause
 */
class GlobalFilter implements Filter {
    private final String escapedStartingValue;
    private final String escapedEndingValue;

    private static final String dateRangeSign = " - ";

    GlobalFilter(String filterValue) {
        if (filterValue.contains(dateRangeSign)) {
            String[] dateRange = filterValue.split(dateRangeSign);
            escapedStartingValue = dateRange[0];
            escapedEndingValue = dateRange[1];
        } else {
            escapedStartingValue = escapeValue(filterValue);
            escapedEndingValue = null;
        }
    }

    String nullOrTrimmedValue(String value) {
        return "\\NULL".equals(value) ? "NULL" : value.trim();
    }

    private String escapeValue(String filterValue) {
        return "%" + nullOrTrimmedValue(filterValue).toLowerCase()
                .replaceAll("~", "~~")
                .replaceAll("%", "~%")
                .replaceAll("_", "~_") + "%";
    }

    @Override
    public Predicate createPredicate(PathBuilder<?> pathBuilder, String attributeName) {
        StringOperation path = Expressions.stringOperation(Ops.TRIM, pathBuilder.get(attributeName));
        if (escapedEndingValue == null) {
            return path.lower().like(escapedStartingValue, '~');
        } else {
            String startHours, endHorus;
            startHours = " 00:00:00";
            endHorus = " 23.59.99";
            return path.goe(escapedStartingValue + startHours).and(path.loe(escapedEndingValue + endHorus));
        }
    }
}