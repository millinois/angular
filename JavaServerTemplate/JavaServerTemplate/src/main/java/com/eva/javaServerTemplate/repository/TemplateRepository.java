package com.eva.javaServerTemplate.repository;

import com.eva.javaServerTemplate.datatablesJpa.qrepository.QDataTablesRepository;
import com.eva.javaServerTemplate.domain.TemplateEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TemplateRepository extends JpaRepository<TemplateEntity, Long>, QDataTablesRepository<TemplateEntity, Long> {
}
