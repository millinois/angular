package com.eva.javaServerTemplate.service;

import com.eva.javaServerTemplate.domain.TemplateEntity;
import com.eva.javaServerTemplate.repository.TemplateRepository;
import org.springframework.stereotype.Service;

@Service
public class TemplateService extends AbstractService<TemplateEntity, Long, TemplateRepository> {
}
