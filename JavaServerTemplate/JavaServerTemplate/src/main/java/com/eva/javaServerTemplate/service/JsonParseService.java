package com.eva.javaServerTemplate.service;

import com.eva.javaServerTemplate.domain.TemplateEntity;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ResourceUtils;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Service
public class JsonParseService {

    @Autowired
    private ObjectMapper mapper = new ObjectMapper();


    @Autowired
    private TemplateService templateService;


    private final String TEMPLATE_FILE = "classpath:jsonData/templateData.json";


    public void createTemplate() throws IOException {
        List<Map<String, Object>> rolesRawListMap = mapper.readValue(ResourceUtils.getFile(TEMPLATE_FILE), new TypeReference<List<Map<String, Object>>>() {
        });
        rolesRawListMap.forEach(rolesRawMap -> {

            String category = ((String) rolesRawMap.get("category"));
            String description = ((String) rolesRawMap.get("description"));
            Boolean passed = ((Boolean) rolesRawMap.get("passed"));


            TemplateEntity templateEntity = TemplateEntity.builder()
                    .category(category)
                    .description(description)
                    .passed(passed).build();
            templateService.save(templateEntity);
        });
    }

    @Transactional
    public void init() throws IOException {
        createTemplate();
    }
}