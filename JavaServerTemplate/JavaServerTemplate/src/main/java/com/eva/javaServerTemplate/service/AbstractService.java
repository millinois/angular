package com.eva.javaServerTemplate.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public abstract class AbstractService<E, Z, T extends JpaRepository<E, Z>> implements CrudService<E, Z> {

    @Autowired
    protected T repository;

    @Override
    public Optional<E> findById(Z id) {
        return repository.findById(id);
    }

    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public E save(E entity) {
        return repository.save(entity);
    }

    @Override
    public List<E> saveAll(List<E> list) {
        return repository.saveAll(list);
    }

    @Override
    public void delete(Z id) {
        repository.deleteById(id);
    }
}
